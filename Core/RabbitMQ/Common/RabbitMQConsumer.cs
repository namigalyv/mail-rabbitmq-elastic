using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Core.RabbitMQ.Common
{
    public class RabbitMQConsumer : RabbitMQService
    {
        public RabbitMQConsumer(string hostname, string userName, string password) : base(hostname, userName, password)
        {
        }

        public bool Consume<T>(T model, string exchangeName, string queue)
        {
            try
            {
                using(IConnection connection = GetRabbitMQConnection())
                {
                    using(var channel = connection.CreateModel())
                    {
                        EventingBasicConsumer consumer=  new EventingBasicConsumer(channel);

                        consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body;
                            var message = System.Text.Encoding.UTF8.GetString(body);
                        };

                        channel.BasicConsume(exchangeName, false, consumer);
                    }
                }

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}